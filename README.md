The Test Maturity Model (TMM) will give a team insight in their maturity in testing. A team will be assessed with 98 checkpoints which can be answered with yes, no or not applicable. These checkpoints have different weights because some checkpoints can be more important than others. The weight varies between 1 and 3. There are seven areas defined in testing were a squad can improve. These area's are:

Ready Work
Alignment
Testware
Test environment
Mastery
Metrics
Reporting

Ready Work is about getting everything clear to set up your tests.

Alignment is about getting alignment within the team and with all parties involved in the chain or End-2-End situation.

Testware is about conserving and traceability of (automated) test scripts.

Test environment is about monitoring and availability of test environments, automated tests and test data.

Mastery is about the knowledge and knowledge sharing in testing.

Metrics is about data which can be gathered and analysed about the test process.

Reporting is about the results of the tests but also about the status of different test tasks and defects.